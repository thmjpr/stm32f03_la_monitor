/**
  ******************************************************************************
  * @file    LIB_Config.h
  * @author  Waveshare Team
  * @version 
  * @date    13-October-2014
  * @brief     This file provides configurations for low layer hardware libraries.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, WAVESHARE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#pragma  once
#ifdef __cplusplus
extern "C" {
#endif
//Macro Definition

/* Includes ------------------------------------------------------------------*/
#include "MacroAndConst.h"
//#include "mxconstants.h"
#include "stm32f0xx_hal.h"


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
#define Bit_SET		GPIO_PIN_SET
#define Bit_RESET	GPIO_PIN_RESET
#define GPIO_WriteBit	HAL_GPIO_WritePin

/* Exported macro ------------------------------------------------------------*/

//Config
#include "ssd1306_config.h"
#include "main.h"
	
/*------------------------------------------------------------------------------------------------------*/

//delay
#include "delay.h"

/*------------------------------------------------------------------------------------------------------*/
//SSD1306
#include "SSD1306.h"
#include "Fonts.h"

//#define SH1106
#define SSD1306

//#define INTERFACE_3WIRE_SPI   //3-wire SPI 
//#define INTERFACE_4WIRE_SPI     //4-wire SPI 
#define INTERFACE_IIC         //I2C

#define __SSD1306_CS_SET()      GPIO_WriteBit(SSD1306_CS_GPIO, SSD1306_CS_PIN, Bit_SET)
#define __SSD1306_CS_CLR()      GPIO_WriteBit(SSD1306_CS_GPIO, SSD1306_CS_PIN, Bit_RESET)

#define __SSD1306_RES_SET()     GPIO_WriteBit(SSD1306_RES_GPIO, SSD1306_RES_PIN, Bit_SET)
#define __SSD1306_RES_CLR()     GPIO_WriteBit(SSD1306_RES_GPIO, SSD1306_RES_PIN, Bit_RESET)

#define __SSD1306_DC_SET()      GPIO_WriteBit(SSD1306_DC_GPIO, SSD1306_DC_PIN, Bit_SET)
#define __SSD1306_DC_CLR()      GPIO_WriteBit(SSD1306_DC_GPIO, SSD1306_DC_PIN, Bit_RESET)

#define __SSD1306_CLK_SET()     GPIO_WriteBit(SSD1306_CLK_GPIO, SSD1306_CLK_PIN, Bit_SET)
#define __SSD1306_CLK_CLR()     GPIO_WriteBit(SSD1306_CLK_GPIO, SSD1306_CLK_PIN, Bit_RESET)

#define __SSD1306_DIN_SET()     GPIO_WriteBit(SSD1306_DIN_GPIO, SSD1306_DIN_PIN, Bit_SET)
#define __SSD1306_DIN_CLR()     GPIO_WriteBit(SSD1306_DIN_GPIO, SSD1306_DIN_PIN, Bit_RESET)

#define __SSD1306_WRITE_BYTE(DATA) spi_read_write_byte(&hspi2, DATA)
/*------------------------------------------------------------------------------------------------------*/
//I2C
#include "IIC.h"

// i2c1 port
//#define SSD1306_I2C_PORT      &hi2c1

// SSD1306 I2C address 
#define SSD1306_I2C_ADDR        0x78
	
#define IIC_SCL_PIN         SCL_Pin
#define IIC_SDA_PIN         SDA_Pin

#define IIC_SCL_GPIO        SCL_GPIO_Port
#define IIC_SDA_GPIO        SDA_GPIO_Port

#define __IIC_SCL_SET()     GPIO_WriteBit(IIC_SCL_GPIO, IIC_SCL_PIN, Bit_SET)
#define __IIC_SCL_CLR()     GPIO_WriteBit(IIC_SCL_GPIO, IIC_SCL_PIN, Bit_RESET)

#define __IIC_SDA_SET()		GPIO_WriteBit(IIC_SDA_GPIO, IIC_SDA_PIN, Bit_SET)
#define __IIC_SDA_CLR()     GPIO_WriteBit(IIC_SDA_GPIO, IIC_SDA_PIN, Bit_RESET)

#define __IIC_SDA_IN()     	do { \
								GPIO_InitTypeDef tGPIO; \
								tGPIO.Pin = IIC_SDA_PIN; \
								tGPIO.Speed = GPIO_SPEED_FREQ_HIGH; \
								tGPIO.Mode = GPIO_MODE_INPUT; \
								HAL_GPIO_Init(IIC_SDA_GPIO, &tGPIO); \
							}while(0)				

#define __IIC_SDA_OUT() 	do { \
								GPIO_InitTypeDef tGPIO; \
								tGPIO.Pin = IIC_SDA_PIN; \
								tGPIO.Speed = GPIO_SPEED_FREQ_HIGH; \
								tGPIO.Mode = GPIO_MODE_OUTPUT_PP; \
								HAL_GPIO_Init(IIC_SDA_GPIO, &tGPIO); \
							}while(0)   
								

#define __IIC_SDA_READ()    HAL_GPIO_ReadPin(IIC_SDA_GPIO, IIC_SDA_PIN)

/*------------------------------------------------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */

#ifdef __cplusplus
}
#endif
/*-------------------------------END OF FILE-------------------------------*/

