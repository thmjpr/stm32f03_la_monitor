/**
  ******************************************************************************
  * @file    xxx.c 
  * @author  Waveshare Team
  * @version 
  * @date    xx-xx-2014
  * @brief   xxxxx.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, WAVESHARE SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "LIB_Config.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/

static void device_init(void);
static void driver_init(void);
static void port_init(void);

#ifdef INTERFACE_4WIRE_SPI
static void spi2_init(void);
#endif
/* Private functions ---------------------------------------------------------*/


/**
  * @brief  System initialization.
  * @param  None
  * @retval  None
  */
void OLED_init(void)
{
	int clock = HAL_RCC_GetHCLKFreq();
	delay_init(clock);
    device_init();
    driver_init();
}


/**
 * @brief 
 * @param 
 * @retval 
 */
static void device_init(void)
{
	port_init();
#ifdef INTERFACE_4WIRE_SPI
	spi2_init();
#endif
	iic_init();
}

/**
  * @brief  driver initialization.
  * @param  None
  * @retval None
  */
static void driver_init(void)
{
	ssd1306_init();
}


static void port_init(void) 
{
	GPIO_InitTypeDef tGPIO;
	
	//???? RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE); 
	
	//----------------------------------------------------------------------------------
	//SPI
#ifdef INTERFACE_4WIRE_SPI
	tGPIO.Pin = SSD1306_DIN_PIN;	 //SCK  MISO	MOSI		 		 
	tGPIO.Speed = GPIO_SPEED_FREQ_LOW;
	tGPIO.Mode = GPIO_MODE_AF_PP;			//Alternate function push pull output
	HAL_GPIO_Init(SSD1306_DIN_GPIO, &tGPIO);
	
	tGPIO.Pin = SSD1306_CLK_PIN;
	tGPIO.Speed = GPIO_SPEED_FREQ_LOW;
	tGPIO.Mode = GPIO_MODE_AF_PP;
	HAL_GPIO_Init(SSD1306_CLK_GPIO, &tGPIO);
	
#elif defined(INTERFACE_3WIRE_SPI)
	tGPIO.GPIO_Pin = SSD1306_CLK_PIN;	 //SCK		 		 
	tGPIO.GPIO_Speed = GPIO_Speed_50MHz;
	tGPIO.GPIO_Mode = GPIO_Mode_Out_PP; 	
	GPIO_Init(GPIOB, &tGPIO);
	
	tGPIO.GPIO_Pin = SSD1306_DIN_PIN;	 //MOSI		 		 
	tGPIO.GPIO_Speed = GPIO_Speed_50MHz;
	tGPIO.GPIO_Mode = GPIO_Mode_Out_PP; 	
	GPIO_Init(GPIOB, &tGPIO);
		
	//----------------------------------------------------------------------------------
	
	//SSD1306 OLED
	tGPIO.Pin = SSD1306_CS_PIN;     // CS				 
	tGPIO.Speed = GPIO_SPEED_FREQ_LOW;
	tGPIO.Mode = GPIO_MODE_OUTPUT_PP;	
	HAL_GPIO_Init(GPIOB, &tGPIO);
	
	tGPIO.Pin = SSD1306_RES_PIN | SSD1306_DC_PIN;     // RES D/C 				 
	tGPIO.Speed = GPIO_SPEED_LOW;
	tGPIO.Mode = GPIO_MODE_OUTPUT_PP;	
	HAL_GPIO_Init(GPIOC, &tGPIO);
#endif
	
	//----------------------------------------------------------------------------------
	//I2C
#ifdef INTERFACE_IIC
	tGPIO.Pin = IIC_SCL_PIN | IIC_SDA_PIN;
	tGPIO.Mode = GPIO_MODE_OUTPUT_PP; //GPIO_Mode_Out_PP
	tGPIO.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(IIC_SCL_GPIO, &tGPIO);
#endif
	//----------------------------------------------------------------------------------
	
}

/**
  * @brief  SPI initialization.
  * @param  None
  * @retval None
  */
#ifdef INTERFACE_4WIRE_SPI
static void spi2_init(void)
{
	//SPI_InitTypeDef tSPI;

	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);
/*
	tSPI.Direction = full_d;
	tSPI.Mode = Mode_Master;		
	tSPI.DataSize = SPI_DataSize_8b;		
	tSPI.CPOL = SPI_CPOL_High;		
	tSPI.CPHA = SPI_CPHA_2Edge;	
	tSPI.NSS = NSS_Soft;		
	tSPI.BaudRatePrescaler = BaudRatePrescaler_8;		
	tSPI.FirstBit = FirstBit_MSB;	
	tSPI.CRCPolynomial = 7;	
	//SPI_Init(SPI2, &tSPI);
	HAL_SPI_Init(&hspi2) 
	//SPI_Cmd(SPI2, ENABLE); ???*/
}
#endif



/*-------------------------------END OF FILE-------------------------------*/
