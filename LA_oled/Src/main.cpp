/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "tim.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
//#include "stdbool.h"
#include "printf.h"
#include "SSD1306.h"
#include "Fonts.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

namespace Cable
{
	enum class State		//class would require cable::state::xx
	{
		Unplugged,
		Plugged_ok,
		Plugged_reverse
	};
	
	enum class Channel
	{
		A = ADC_CC0_Pin,
		B = ADC_CC1_Pin,
		C = ADC_CC2_Pin,
		D = ADC_CC3_Pin
	};
}


namespace Trig
{
	enum class Level
	{
		TTL,
	};
}


typedef enum
{
	TTL,
	LVDS,
	ECL,
	ZERO_V,
	CMOS1_8,
	CMOS2_5,
	CMOS5_0,
}trigger_level;

Trig::Level trigger_lvl[2], trigger_lvl_last[2] = {};
Cable::State cable_status[NUM_CABLES], cable_status_last[NUM_CABLES] = { };

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

void green_led(bool state);
Cable::State get_cable_status(Cable::Channel c_adc);
Trig::Level get_trigger_level(adc_ch c_adc);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

	/* USER CODE BEGIN 1 */
	char build_date[] = __DATE__;
	uint8_t buff[32];

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_ADC_Init();
	MX_TIM17_Init();

	/* USER CODE BEGIN 2 */
	green_led(true);

	//Start the timer running
	HAL_TIM_Base_Start(&htim17);
	OLED_init();    				//Enable OLED
		
	//Get initial cable status
	update_adc();
	cal_vref_int(); 		//calibrate to determine input voltage
	cable_status[0] = get_cable_status(Cable::Channel::A);
	cable_status[1] = get_cable_status(Cable::Channel::B);
	cable_status[2] = get_cable_status(Cable::Channel::C);
	cable_status[3] = get_cable_status(Cable::Channel::D);
	cable_status_last[0] = cable_status[0];
	cable_status_last[1] = cable_status[1];
	cable_status_last[2] = cable_status[2];
	cable_status_last[3] = cable_status[3];
	
	//Get initial Vref voltages
	trigger_lvl[0] = get_trigger_level(ADC_VREF0);
	trigger_lvl[1] = get_trigger_level(ADC_VREF1);
	trigger_lvl_last[0] = trigger_lvl[0];
	trigger_lvl_last[1] = trigger_lvl[1];
	
	//Show splash screen
	ssd1306_draw_bitmap(0, 0, icon_splash_screen, 128, 32);
	ssd1306_update();
	HAL_Delay(2000);
	ssd1306_set_contrast(50);
	
	//try again -bug
	OLED_init();
	ssd1306_draw_bitmap(0, 0, icon_splash_screen, 128, 32);
	ssd1306_update();
	HAL_Delay(2500);
	ssd1306_set_contrast(50);
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

	while (1)
	{
		update_adc();
		cal_vref_int();		//calibrate to determine input voltage
		
		float vref0 = get_voltage(ADC_VREF0) * 10;	//Trigger voltage is 10x Vref (-1.5 to +1.5V is -15 to +15)
		float vref1 = get_voltage(ADC_VREF1) * 10;
		
		ssd1306_clear_screen(0);		//Blank display memory
		
		//If pod C, D are not connected, could simplify the screen so only pod A, B info is shown
		
		//Measure 2.5V rail then calculate what the pullup resistors should be (not sure why I didnt use 3.3V here)
		
		int str_len = 0;
		//if vref < 0.005 then show "<0" as ADC cannot read negative voltage
		if(vref0 < 0.005)
			str_len = sprintf((char*)buff, "   <0.00V ");
		else
		{
			if (vref0 < 9.999)	//2.xxxV
				str_len = sprintf((char*)buff, "   %0.3fV ", vref0);
			else				//10.xV
				str_len = sprintf((char*)buff, "   %0.2fV ", vref0);			
		}

		if (vref1 < 0.005)
			sprintf((char*)buff + str_len, "   <0.00V ");
		else
		{
			if (vref1 < 9.999)
				sprintf((char*)buff + str_len, "   %0.3fV", vref1);
			else
				sprintf((char*)buff + str_len, "   %0.2fV", vref1);			
		}
		ssd1306_display_string(0, 19, buff, FONT_MED, 1);
		
		//
		char chan = 'A';
		str_len = 0;
		
		for(int i = 0 ; i < NUM_CABLES ; i++)
		{
			if (cable_status[i] == Cable::State::Plugged_ok)
				str_len += sprintf((char*)buff + str_len, " %c: %c", chan, CHK_MARK);
			else if (cable_status[i] == Cable::State::Plugged_reverse)
				str_len += sprintf((char*)buff + str_len, " %c: %c", chan, CON_WRONG);
			else	//Cable::State::Unplugged
				str_len += sprintf((char*)buff +str_len, " %c: -", chan);
			chan++;
		}	
		ssd1306_display_string(0, 0, buff, FONT_MED, 1);
		
		
		//Scan for cable changes
		cable_status[0] = get_cable_status(Cable::Channel::A);
		cable_status[1] = get_cable_status(Cable::Channel::B);
		cable_status[2] = get_cable_status(Cable::Channel::C);
		cable_status[3] = get_cable_status(Cable::Channel::D);
		
		for (int i = 0; i < NUM_CABLES; i++)
		{		
			if (cable_status[i] != cable_status_last[i])
			{
				switch (cable_status[i])
				{
				case Cable::State::Plugged_ok:
					//"x pluggeed OK
					break;
				case Cable::State::Unplugged:
					break;
				case Cable::State::Plugged_reverse:
					break;
				}
			
				cable_status_last[i] = cable_status[i];
			}
		}
		
		ssd1306_update();
		delay_ms(500);	
		green_led(!green_led_state());
	}
	
	
	while(0)
	{
		green_led(!green_led_state());
		update_adc();
		cal_vref_int(); 		//calibrate to determine input voltage
		
		ssd1306_clear_screen(0);
		
		float cc0 = get_voltage(ADC_CC0);
		float cc1 = get_voltage(ADC_CC1);
		float cc2 = get_voltage(ADC_CC2);
		float cc3 = get_voltage(ADC_CC3);
		
		
		sprintf((char*)buff, "%0.3fV ", cc0);
		ssd1306_display_string(0, 0, buff, FONT_MED, 1);
		sprintf((char*)buff, "%0.3fV ", cc1);
		ssd1306_display_string(50, 0, buff, FONT_MED, 1);
		sprintf((char*)buff, "%0.3fV ", cc2);
		ssd1306_display_string(0, 16, buff, FONT_MED, 1);
		sprintf((char*)buff, "%0.3fV ", cc3);
		ssd1306_display_string(50, 16, buff, FONT_MED, 1);
		
		
		ssd1306_update();
		
		//vref0 = adc_current_val(ADC_VREF0);
		//vref1 = adc_current_val(ADC_VREF1);
		//ref_int = adc_current_val(ADC_INT_REF);
		
		
		/*
		sprintf((char*)buff, "A: ");
		ssd1306_display_string(0, 0, buff, FONT_LARGE, 1);
		*/
		//A B C D = N/C
		
		//Scan for cable changes
		int i;
		for (i = 0; i < 4; i++)
		{
			if (cable_status[i] != cable_status_last[i])
			{
				switch (cable_status[i])
				{
				case Cable::State::Plugged_ok:
					//"x pluggeed OK
					break;
				case Cable::State::Unplugged:
					break;
				case Cable::State::Plugged_reverse:
					break;
				}
			
				cable_status_last[i] = cable_status[i];
			}
		}
		
		//Scan for trigger changes
		for (i = 0; i < 2; i++)
		{
			if (trigger_lvl[i] != trigger_lvl_last[i])
			{
				//Trigger voltage changed
			
				trigger_lvl_last[i] = trigger_lvl[i];
			}
		}
		
		
		
		HAL_Delay(500);
	  
	  
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

	}
	/* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Initializes the CPU, AHB and APB busses clocks 
	*/
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL10;
	RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB busses clocks 
	*/
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
	                            |RCC_CLOCKTYPE_PCLK1;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV16;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
	{
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */

//Read voltage and determine if cable connected
//
Cable::State get_cable_status(Cable::Channel chan)
{
	int tmp = 0;
	//whyyyyyyyyyy, dont do this
	switch(chan)
	{
	case Cable::Channel::A:
		tmp = 2;  break;
	case Cable::Channel::B:
		tmp = 3;  break;
	case Cable::Channel::C:
		tmp = 4; break;
	case Cable::Channel::D:
		tmp = 5; break;
	}
	
	
 	float adc = get_voltage((adc_ch)tmp);		//pin voltage
    float v25 = get_voltage(adc_ch::ADC_2V5);	//2.5V rail voltage
	adc = adc / v25;							//percentage of 2.5V rail
	
	//Connect and inverted?
	if (adc < 0.338)
	{
		return Cable::State::Plugged_ok;
	}
	else if (adc < 0.5)
	{
		return Cable::State::Plugged_reverse;
	}
	
	else if (adc < 0.9)
	{
		return Cable::State::Unplugged;
	}
	//No cable connected
	else
	{
		return Cable::State::Unplugged;
	}
}


//
//Voltage can only go to 0V! so need to put a resistor + diode, and measure if sub 0V changes the reading at all

Trig::Level get_trigger_level(adc_ch i_adc)
{
	float adc = get_voltage(i_adc);
	
	
	if (adc < 0.1)
	{
		//ECL or 0V
	}
	else if (adc < (0.9 + 0.1))
	{
		//CMOS1.8
	}
	
	else if (adc < (1.2 + 0.1))
	{
		//LVDS
	}

	else if (adc < (1.25 + 0.1))
	{
		//CMOS2.5
	}

	else if (adc < (1.4 + 0.1))
	{
		//TTL 1.4
	}
	
	else if (adc < (1.65 + 0.1))
	{
		//LVDS
	}
	
	else // > 2V or whatevers
		{
			//CMOS5
		}
		
	return Trig::Level::TTL;
}



/**
  * @brief  Output of green LED next to processor
  * @param  On or Off state of LED
  * @retval None
  */

void green_led(bool state)
{
	if (state)
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
	else
		HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
}


/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */

void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler */
	  /* User can add his own implementation to report the HAL error return state */
	while (1) 
	{
		green_led(true);
	}
	/* USER CODE END Error_Handler */ 
}

#define USE_FULL_ASSERT

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	  /* User can add his own implementation to report the file name and line number,
	    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
